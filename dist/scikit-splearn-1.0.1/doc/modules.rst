splearn
=======

.. toctree::
   :maxdepth: 2

   automaton
   spectral
   hankel
   splearn.datasets
