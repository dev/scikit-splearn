spectral module
===============

.. automodule:: spectral
    :members: Learning, Spectral
    :undoc-members:
    :show-inheritance:
