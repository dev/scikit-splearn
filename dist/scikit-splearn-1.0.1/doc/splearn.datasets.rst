splearn.datasets
================


base
----

.. automodule:: splearn.datasets.base
    :members: load_data_sample
    :undoc-members:
    :show-inheritance:


data_sample
-----------

.. automodule:: splearn.datasets.data_sample
    :members: DataSample , Splearn_array
    :undoc-members:
    :show-inheritance:



