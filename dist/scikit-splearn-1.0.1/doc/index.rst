.. scikit-splearn documentation master file, created by
   sphinx-quickstart on Fri Nov  6 17:21:52 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scikit-splearn's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 3

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

