from splearn.automaton import Automaton
from splearn.spectral import Learning
from splearn.spectral import Spectral
from splearn.hankel import Hankel
__version__ = "1.0.1"
