from splearn.automaton import Automaton
from graphviz import Source
from splearn.hankel import Hankel

title = "Simple Example"
input_file = 'simple_example'
A = Automaton.SimpleExample()
formats = ['json', 'yaml']
words = [[], [0], [0, 0], [1], [1, 1], [0, 1, 0], [1, 0, 1]]
d_str = "Value for word \"{:s}\" = {:.2f}"
# read simple automata in each format and write hankel
for f in formats:
    #A = Automaton.read(input_file + "." + f, format=f)
    Automaton.write(A, input_file + '.' + f, format = f)
    dot = A.get_dot(title = title)
    src = Source(dot)
    src.render(input_file + '.' + f + '.gv', view=False)
    # Display some values on words
    print("First Automata")
    for w in words:
        print(d_str.format(str(w), A.val(w)))
    H = A.to_hankel([(), (0,), (1,)], [(), (0,), (1,)])
    Hankel.write(H, input_file + "_hankel" + "." + f, format=f)
  
# read hankel and generate Automata
for f in formats:
    H = Hankel.read(input_file + "_hankel" + "." + f, format = f)
    A = H.to_automaton(2)
    dot = A.get_dot(title = title)
    src = Source(dot)
    src.render(input_file + '-2.' + f + '.gv', view=False)
    # Display some values on words
    print("Second Automata")
    for w in words:
        print(d_str.format(str(w), A.val(w)))
    

